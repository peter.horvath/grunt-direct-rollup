# grunt-direct-rollup
Async Grunt task to call rollup like from the command line, but without forking a new node

You can call rollup from Grunt, as an asynchronous Grunt task, but as you learned it from
the command line.

It is called as an ordinary Grunt task, without forking a new node instance. Despite that,
it uses your local rollup.config (or any rollup.config what you specified), so you can use
with from the command line called rollups directly.

An example:

    var rollupConfig = require('./rollup.config');

    module.exports = function(grunt) {
      'use strict';
      
      grunt.initConfig({
      
        rollup : {
          options : rollupConfig;
        }
      
      }
      
      grunt.loadNpmTasks("grunt-direct-rollup");
    };
