'use strict';

var rollup = require('rollup');
var path = require('path');

function onBundleGenerateReady(grunt, bundle, options, result) {
    var code = result.code;

    if (options.sourcemap === true) {
        var sourcemapOutPath = options.dest + '.map';
        grunt.file.write(sourcemapOutPath, result.map.toString());
        code += "\n//# sourceMappingURL=" + path.basename(sourcemapOutPath);
    } else if (options.sourcemap === "inline") {
        code += "\n//# sourceMappingURL=" + result.map.toUrl();
    }

    grunt.file.write(options.output.file, code);
}

function invokeBundle(grunt, bundle, options, done) {
	console.log("invokeBundle");
    var sourcemapFile = options.sourcemapFile;
    if (!sourcemapFile && options.sourcemapRelativePaths) {
        sourcemapFile = path.resolve(options.dest);
    }

    bundle.generate({
        format: options.format,
        exports: options.exports,
        moduleId: options.moduleId,
        name: options.name,
        globals: options.globals,
        indent: options.indent,
        strict: options.strict,
        banner: options.banner,
        footer: options.footer,
        intro: options.intro,
        outro: options.outro,
        sourcemap: options.sourcemap,
        sourcemapFile: sourcemapFile
    }).then(function(result) {
    	onBundleGenerateReady(grunt, bundle, options, result);
    	done();
    })
    .catch(function(error) {
    	grunt.fail.warn(error);
    	done(1);
    });
}

function invokeRollup(grunt, ctx) {
	var done = ctx.async();
	
    var options = ctx.options({
        external: [],
        format: 'es',
        exports: 'auto',
        moduleId: null,
        name: null,
        globals: {},
        indent: true,
        strict: true,
        banner: null,
        footer: null,
        intro: null,
        outro: null,
        plugins: [],
        sourcemap: false,
        sourcemapFile: null,
        sourcemapRelativePaths: false,
        onwarn: false
    });

    console.log("options: " + JSON.stringify(options));

    if (!options.input) {
        grunt.fail.warn('No entry point specified.');
    }

    if (!options.output) {
        grunt.fail.warn('No dest specified.');
    }
    
    if (typeof options.plugins === 'function') {
        options.plugins = options.plugins();
    }

    console.log("calling rollup.rollup({...})");
    rollup.rollup({
        input: options.input,
        output: options.output,
        sourcemap: options.sourcemap,
        treeshake: options.treeshake,
        external: options.external,
        plugins: options.plugins,
        context: options.context,
        moduleContext: options.moduleContext,
        onwarn: options.onwarn
    })
    .then(function(bundle){
    	invokeBundle(grunt, bundle, options, done);
    	//ctx.done();
    })
    .catch(function(error) {
    	grunt.fail.warn(error);
    	done(1);
    });
}

function registerRollupTask(grunt) {
    grunt.registerTask('rollup', 'Grunt plugin for rollup - next-generation ES6 module bundler', function() {
    	invokeRollup(grunt, this);
    });
}

module.exports = registerRollupTask;